The project creates a basic MIPS Single Cycle Processor with vital components for datapath and control path. The processor receives opcodes that define specific tasks and data to be manipulated through the cycle and evaluates the speeds at which commands are performed. 

The project folder contains source code for all necessary parts of the processor and project files necessary to be edited and tested within Xilinx 14.7.
Included is also a test frame that inserts specific values into the inputs and can also be viewed in the project's documentation.