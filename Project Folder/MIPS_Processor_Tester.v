`timescale 1ns / 1ps
module MIPS_Processor_Tester;

	// Inputs
	reg clk;

	// Outputs
	wire [25:0] Zero_Flag;

	// Instantiate the Unit Under Test (UUT)
	MIPS_Processor uut (
		.clk(clk), 
		.Zero_Flag(Zero_Flag)
	);
	
	//define symbolic constants for instruction machine codes
	parameter	NOP					=	32'h00000020,
			add_$2_$0_$0				=	32'h00001020,
			add_$3_$0_$0				=	32'h00001820,
			slt_$4_$2_$5				=	32'h0045202a,
			beq_$4_$0_exit_loop		= 	32'h10800006,
			add_$4_$2_$2				=	32'h00422020,
			add_$6_$4_$4				=	32'h00843020,
			lw_$7_DMEM_$6				=	32'h8cc70000,
			add_$3_$3_$7				=	32'h00671820,
			add_$2_$2_$1				=	32'h00411020,
			j_0x0000000C				=	32'h08000003,
			sw_$3_DMEM_$0				=	32'hac030000;
	//indexing and counter variables
	integer i,j;
	//clock pulse generation
	always #5 clk = ~clk;
	/********************************************************************/
	//simulation stopping code block
	always
		//add a time delay so instructions are checked every 10 time units
		#10 if(uut.MC0.IMEM.Instruction == sw_$3_DMEM_$0)
			begin
			#5
			$display("Final Contents:");
			$display(" $1 = %h, $2 = %h, $3 = %h",
					uut.MC0.eu1.RF32.RF[1],
					uut.MC0.eu1.RF32.RF[2],
					uut.MC0.eu1.RF32.RF[3]);
			$display(" $5 = %h, $6 = %h, $7 = %h",
					uut.MC0.eu1.RF32.RF[5],
					uut.MC0.eu1.RF32.RF[6],
					uut.MC0.eu1.RF32.RF[7]);
			$display(" DMEM[0]=%h%h%h%h",
					uut.MC0.DMEM.DM[0],
					uut.MC0.DMEM.DM[1],
					uut.MC0.DMEM.DM[2],
					uut.MC0.DMEM.DM[3]);
			#20 $finish;
			end
	/********************************************************************/
	//console output message control code block
	always 	@(posedge clk)
	begin
		$timeformat( -9, 1, " ns", 8);
		$display("At time %t", $time);
		#1;
		//check for first instruction
		case(uut.MC0.IMEM.Instruction)
			NOP:	begin
				$display("NOP was executed");
				end
			add_$2_$0_$0:	begin
				$display("add $2, $0, $0 was executed");
				$display("Register values before");
				$display("\t$0 = %h, $0 = %h",
					uut.MC0.eu1.RF32.RF[0],
					uut.MC0.eu1.RF32.RF[0]);
				@(negedge clk) #1;
				$display("Destination register content after");
				$display("\t$2 = %h",
					uut.MC0.eu1.RF32.RF[2]);
				end
			add_$3_$0_$0:	begin
				$display("add $3, $0, $0 was executed");
				$display("Register values before");
				$display("\t$0 = %h, $0 = %h",
					uut.MC0.eu1.RF32.RF[0],
					uut.MC0.eu1.RF32.RF[0]);
				@(negedge clk) #1;
				$display("Destination register content after");
				$display("\t$3 = %h",
					uut.MC0.eu1.RF32.RF[3]);
				end
			slt_$4_$2_$5: begin
				$display("slt $4, $2, $5 was executed");
				$display("Register values before");
				$display("\t$2 = %h, $5 = %h",
					uut.MC0.eu1.RF32.RF[2],
					uut.MC0.eu1.RF32.RF[5]);
				@(negedge clk) #1;
				$display("Destination register content after");
				$display("\t$4 = %h",
					uut.MC0.eu1.RF32.RF[4]);
				end
			beq_$4_$0_exit_loop: begin
				$display("beq $4, $0, exit_loop was executed");
				if(uut.PC0_Branch1)
					$display("Branch was taken");		
				else
					$display("Branch was not taken...");
				end
			add_$4_$2_$2:	begin
				$display("add $4, $2, $2 was executed");
				$display("Register values before");
				$display("\t$2 = %h, $2 = %h",
					uut.MC0.eu1.RF32.RF[2],
					uut.MC0.eu1.RF32.RF[2]);
				@(negedge clk) #1;
				$display("Destination register content after");
				$display("\t$4 = %h",
					uut.MC0.eu1.RF32.RF[4]);
				end
			add_$6_$4_$4:	begin
				$display("add $6, $4, $4 was executed");
				$display("Register values before");
				$display("\t$4 = %h, $4 = %h",
					uut.MC0.eu1.RF32.RF[4],
					uut.MC0.eu1.RF32.RF[4]);
				@(negedge clk) #1;
				$display("Destination register content after");
				$display("\t$6 = %h",
					uut.MC0.eu1.RF32.RF[6]);
				end
			lw_$7_DMEM_$6: begin
				$display("lw $7, DMEM($6) was executed");
				$display("DMEM value");
				$display("\t$DMEM($6) = %h",
					uut.MC0.DMEM.DM[
					uut.MC0.eu1.RF32.RF[6] ]);
				@(negedge clk) #1;
				$display("Destination register content after");
				$display("\t$7 = %h",
					uut.MC0.eu1.RF32.RF[7]);
				end
			add_$3_$3_$7:	begin
				$display("add $3, $3, $7 was executed");
				$display("Register values before");
				$display("\t$3 = %h, $7 = %h",
					uut.MC0.eu1.RF32.RF[3],
					uut.MC0.eu1.RF32.RF[7]);
				@(negedge clk) #1;
				$display("Destination register content after");
				$display("\t$3 = %h",
					uut.MC0.eu1.RF32.RF[3]);
				end
			add_$2_$2_$1:	begin
				$display("add $2, $2, $1 was executed");
				$display("Register values before");
				$display("\t$2 = %h, $1 = %h",
					uut.MC0.eu1.RF32.RF[2],
					uut.MC0.eu1.RF32.RF[1]);
				@(negedge clk) #1;
				$display("Destination register content after");
				$display("\t$2 = %h",
					uut.MC0.eu1.RF32.RF[2]);
				end
			j_0x0000000C:	begin
				$display("Jump to IMEM[0x0000000C] was executed");
				end
			sw_$3_DMEM_$0: begin
				$display("sw $3, DMEM($0) was executed");
				$display("Register values before");
				$display("\t$3 = %h",
					uut.MC0.eu1.RF32.RF[3]);
				
				@(negedge clk) #1;
				$display("Destination DMEM content after");
				$display("\tDMEM($0) = %h",
					uut.MC0.DMEM.DM[0]);
				end
			default: begin
				$display("Unanticipated instruction has Executed!!!");
				end
		endcase
		$display("");
	end																		
	/***********************************************************/
	//signal initialization code block
	initial begin
		//initialize clock signal
		clk = 0;
		j = 0;
		//initialize registers with values equal to their indices
		for(i = 1; i < 32; i = i + 1)
			uut.MC0.eu1.RF32.RF[i] = i;
		//initialize DMEM with random stuff
		for(i = 0; i < 256; i = i + 1)
			uut.MC0.DMEM.DM[i] = $random;
		//initialize PC
		uut.PC.PC_Out = 0;
		
		//define program in IMeM
		//add $0, $0, $0
		{uut.MC0.IMEM.IM[0],uut.MC0.IMEM.IM[1],
		 uut.MC0.IMEM.IM[2],uut.MC0.IMEM.IM[3]}
		 = NOP;


		//add $2, $0, $0
		{uut.MC0.IMEM.IM[4],uut.MC0.IMEM.IM[5],
		 uut.MC0.IMEM.IM[6],uut.MC0.IMEM.IM[7]}
		 = add_$2_$0_$0;


		//add $3, $0, $0
		{uut.MC0.IMEM.IM[8],uut.MC0.IMEM.IM[9],
		 uut.MC0.IMEM.IM[10],uut.MC0.IMEM.IM[11]}
		 = add_$3_$0_$0;


		//slt $4, $2, $5	
		{uut.MC0.IMEM.IM[12],uut.MC0.IMEM.IM[13],
		 uut.MC0.IMEM.IM[14],uut.MC0.IMEM.IM[15]}
		 = slt_$4_$2_$5;


		//beq $4, $0, exit_loop	
		{uut.MC0.IMEM.IM[16],uut.MC0.IMEM.IM[17],
		 uut.MC0.IMEM.IM[18],uut.MC0.IMEM.IM[19]}
		 = beq_$4_$0_exit_loop;


		//add_$4, $2, $2
		{uut.MC0.IMEM.IM[20],uut.MC0.IMEM.IM[21],
		 uut.MC0.IMEM.IM[22],uut.MC0.IMEM.IM[23]}
		 = add_$4_$2_$2;

		//add $6, $4, $4
		{uut.MC0.IMEM.IM[24],uut.MC0.IMEM.IM[25],
		 uut.MC0.IMEM.IM[26],uut.MC0.IMEM.IM[27]}
		 = add_$6_$4_$4;
		
		//lw $7, DMEM($6)
		{uut.MC0.IMEM.IM[28],uut.MC0.IMEM.IM[29],
		 uut.MC0.IMEM.IM[30],uut.MC0.IMEM.IM[31]}
		 = lw_$7_DMEM_$6;
		
		//add $3, $3, $7
		{uut.MC0.IMEM.IM[32],uut.MC0.IMEM.IM[33],
		 uut.MC0.IMEM.IM[34],uut.MC0.IMEM.IM[35]}
		 = add_$3_$3_$7;


		//add $2, $2, $1
		{uut.MC0.IMEM.IM[36],uut.MC0.IMEM.IM[37],
		 uut.MC0.IMEM.IM[38],uut.MC0.IMEM.IM[39]}
		 = add_$2_$2_$1;


		//j 0x0000000C
		{uut.MC0.IMEM.IM[40],uut.MC0.IMEM.IM[41],
		 uut.MC0.IMEM.IM[42],uut.MC0.IMEM.IM[43]}
		 = j_0x0000000C;


		//sw $3, DMEM($0)
		{uut.MC0.IMEM.IM[44],uut.MC0.IMEM.IM[45],
		 uut.MC0.IMEM.IM[46],uut.MC0.IMEM.IM[47]}
		 = sw_$3_DMEM_$0;
	end
endmodule
