`timescale 1ns / 1ps
module ALU( A, B, ALU_Ctrl, Zero_Flag, Output);
   input		[31:0]		A, B;
	input		[3:0]			ALU_Ctrl;
	 
	output reg				Zero_Flag;
	output reg signed	[31:0]	Output;
	 
	always @(ALU_Ctrl, A, B) 	begin
		//$display("A: 0x%h	B: 0x%h	ALU_Ctrl: 0x%b", A, B, ALU_Ctrl);
		case(ALU_Ctrl)
		4'b0000:	Output <=	A & B;
		4'b0001:	Output <=	A | B;
		4'b0010:	Output <=	A + B;
		4'b0110:	Output <=	A - B;
		4'b0111:	Output <=  	(A < B)?32'b1:32'b0;
		4'b1100:	Output <=	~( A | B );
		default: Output =	32'bX;
		endcase
		assign Zero_Flag = !Output;
	end

endmodule
