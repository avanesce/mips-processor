/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Andy/Documents/Xilinx Labs/Lab_7_MIPS_Processor/DMEM_256x8.v";
static const char *ng1 = "Writing %h to %h";
static int ng2[] = {1, 0};
static int ng3[] = {32, 0};
static int ng4[] = {24, 0};
static int ng5[] = {0, 0};
static int ng6[] = {2, 0};
static int ng7[] = {3, 0};



static void Always_11_0(char *t0)
{
    char t7[8];
    char t29[8];
    char t30[8];
    char t37[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    char *t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    char *t26;
    char *t27;
    char *t28;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    char *t38;
    int t39;
    char *t40;
    int t41;
    int t42;
    int t43;
    int t44;

LAB0:    t1 = (t0 + 3488U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(11, ng0);
    t2 = (t0 + 4552);
    *((int *)t2) = 1;
    t3 = (t0 + 3520);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(12, ng0);

LAB5:    xsi_set_current_line(13, ng0);
    t4 = (t0 + 1688U);
    t5 = *((char **)t4);
    t4 = (t0 + 1528U);
    t6 = *((char **)t4);
    xsi_vlogfile_write(1, 0, 0, ng1, 3, t0, (char)118, t5, 32, (char)118, t6, 8);
    xsi_set_current_line(14, ng0);
    t2 = (t0 + 1208U);
    t3 = *((char **)t2);
    t2 = ((char*)((ng2)));
    memset(t7, 0, 8);
    t4 = (t3 + 4);
    t5 = (t2 + 4);
    t8 = *((unsigned int *)t3);
    t9 = *((unsigned int *)t2);
    t10 = (t8 ^ t9);
    t11 = *((unsigned int *)t4);
    t12 = *((unsigned int *)t5);
    t13 = (t11 ^ t12);
    t14 = (t10 | t13);
    t15 = *((unsigned int *)t4);
    t16 = *((unsigned int *)t5);
    t17 = (t15 | t16);
    t18 = (~(t17));
    t19 = (t14 & t18);
    if (t19 != 0)
        goto LAB9;

LAB6:    if (t17 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t7) = 1;

LAB9:    t20 = (t7 + 4);
    t21 = *((unsigned int *)t20);
    t22 = (~(t21));
    t23 = *((unsigned int *)t7);
    t24 = (t23 & t22);
    t25 = (t24 != 0);
    if (t25 > 0)
        goto LAB10;

LAB11:
LAB12:    goto LAB2;

LAB8:    t6 = (t7 + 4);
    *((unsigned int *)t7) = 1;
    *((unsigned int *)t6) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(15, ng0);

LAB13:    xsi_set_current_line(16, ng0);
    t26 = (t0 + 1688U);
    t27 = *((char **)t26);
    t26 = (t0 + 1528U);
    t28 = *((char **)t26);
    xsi_vlogfile_write(1, 0, 0, ng1, 3, t0, (char)118, t27, 32, (char)118, t28, 8);
    xsi_set_current_line(17, ng0);
    t2 = (t0 + 1688U);
    t3 = *((char **)t2);
    t2 = (t0 + 1648U);
    t4 = (t2 + 72U);
    t5 = *((char **)t4);
    t6 = ((char*)((ng3)));
    t20 = ((char*)((ng4)));
    xsi_vlog_generic_get_part_select_value(t7, 9, t3, t5, 2, t6, 32U, 1, t20, 32U, 1);
    t26 = (t0 + 2248);
    t27 = (t0 + 2248);
    t28 = (t27 + 72U);
    t31 = *((char **)t28);
    t32 = (t0 + 2248);
    t33 = (t32 + 64U);
    t34 = *((char **)t33);
    t35 = (t0 + 1528U);
    t36 = *((char **)t35);
    t35 = ((char*)((ng5)));
    memset(t37, 0, 8);
    xsi_vlog_unsigned_add(t37, 32, t36, 8, t35, 32);
    xsi_vlog_generic_convert_array_indices(t29, t30, t31, t34, 2, 1, t37, 32, 2);
    t38 = (t29 + 4);
    t8 = *((unsigned int *)t38);
    t39 = (!(t8));
    t40 = (t30 + 4);
    t9 = *((unsigned int *)t40);
    t41 = (!(t9));
    t42 = (t39 && t41);
    if (t42 == 1)
        goto LAB14;

LAB15:    xsi_set_current_line(18, ng0);
    t2 = (t0 + 1688U);
    t3 = *((char **)t2);
    memset(t7, 0, 8);
    t2 = (t7 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 16);
    *((unsigned int *)t7) = t9;
    t10 = *((unsigned int *)t4);
    t11 = (t10 >> 16);
    *((unsigned int *)t2) = t11;
    t12 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t12 & 255U);
    t13 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t13 & 255U);
    t5 = (t0 + 2248);
    t6 = (t0 + 2248);
    t20 = (t6 + 72U);
    t26 = *((char **)t20);
    t27 = (t0 + 2248);
    t28 = (t27 + 64U);
    t31 = *((char **)t28);
    t32 = (t0 + 1528U);
    t33 = *((char **)t32);
    t32 = ((char*)((ng2)));
    memset(t37, 0, 8);
    xsi_vlog_unsigned_add(t37, 32, t33, 8, t32, 32);
    xsi_vlog_generic_convert_array_indices(t29, t30, t26, t31, 2, 1, t37, 32, 2);
    t34 = (t29 + 4);
    t14 = *((unsigned int *)t34);
    t39 = (!(t14));
    t35 = (t30 + 4);
    t15 = *((unsigned int *)t35);
    t41 = (!(t15));
    t42 = (t39 && t41);
    if (t42 == 1)
        goto LAB16;

LAB17:    xsi_set_current_line(19, ng0);
    t2 = (t0 + 1688U);
    t3 = *((char **)t2);
    memset(t7, 0, 8);
    t2 = (t7 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 8);
    *((unsigned int *)t7) = t9;
    t10 = *((unsigned int *)t4);
    t11 = (t10 >> 8);
    *((unsigned int *)t2) = t11;
    t12 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t12 & 255U);
    t13 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t13 & 255U);
    t5 = (t0 + 2248);
    t6 = (t0 + 2248);
    t20 = (t6 + 72U);
    t26 = *((char **)t20);
    t27 = (t0 + 2248);
    t28 = (t27 + 64U);
    t31 = *((char **)t28);
    t32 = (t0 + 1528U);
    t33 = *((char **)t32);
    t32 = ((char*)((ng6)));
    memset(t37, 0, 8);
    xsi_vlog_unsigned_add(t37, 32, t33, 8, t32, 32);
    xsi_vlog_generic_convert_array_indices(t29, t30, t26, t31, 2, 1, t37, 32, 2);
    t34 = (t29 + 4);
    t14 = *((unsigned int *)t34);
    t39 = (!(t14));
    t35 = (t30 + 4);
    t15 = *((unsigned int *)t35);
    t41 = (!(t15));
    t42 = (t39 && t41);
    if (t42 == 1)
        goto LAB18;

LAB19:    xsi_set_current_line(20, ng0);
    t2 = (t0 + 1688U);
    t3 = *((char **)t2);
    memset(t7, 0, 8);
    t2 = (t7 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 0);
    *((unsigned int *)t7) = t9;
    t10 = *((unsigned int *)t4);
    t11 = (t10 >> 0);
    *((unsigned int *)t2) = t11;
    t12 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t12 & 255U);
    t13 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t13 & 255U);
    t5 = (t0 + 2248);
    t6 = (t0 + 2248);
    t20 = (t6 + 72U);
    t26 = *((char **)t20);
    t27 = (t0 + 2248);
    t28 = (t27 + 64U);
    t31 = *((char **)t28);
    t32 = (t0 + 1528U);
    t33 = *((char **)t32);
    t32 = ((char*)((ng7)));
    memset(t37, 0, 8);
    xsi_vlog_unsigned_add(t37, 32, t33, 8, t32, 32);
    xsi_vlog_generic_convert_array_indices(t29, t30, t26, t31, 2, 1, t37, 32, 2);
    t34 = (t29 + 4);
    t14 = *((unsigned int *)t34);
    t39 = (!(t14));
    t35 = (t30 + 4);
    t15 = *((unsigned int *)t35);
    t41 = (!(t15));
    t42 = (t39 && t41);
    if (t42 == 1)
        goto LAB20;

LAB21:    goto LAB12;

LAB14:    t10 = *((unsigned int *)t29);
    t11 = *((unsigned int *)t30);
    t43 = (t10 - t11);
    t44 = (t43 + 1);
    xsi_vlogvar_wait_assign_value(t26, t7, 0, *((unsigned int *)t30), t44, 0LL);
    goto LAB15;

LAB16:    t16 = *((unsigned int *)t29);
    t17 = *((unsigned int *)t30);
    t43 = (t16 - t17);
    t44 = (t43 + 1);
    xsi_vlogvar_wait_assign_value(t5, t7, 0, *((unsigned int *)t30), t44, 0LL);
    goto LAB17;

LAB18:    t16 = *((unsigned int *)t29);
    t17 = *((unsigned int *)t30);
    t43 = (t16 - t17);
    t44 = (t43 + 1);
    xsi_vlogvar_wait_assign_value(t5, t7, 0, *((unsigned int *)t30), t44, 0LL);
    goto LAB19;

LAB20:    t16 = *((unsigned int *)t29);
    t17 = *((unsigned int *)t30);
    t43 = (t16 - t17);
    t44 = (t43 + 1);
    xsi_vlogvar_wait_assign_value(t5, t7, 0, *((unsigned int *)t30), t44, 0LL);
    goto LAB21;

}

static void Always_24_1(char *t0)
{
    char t6[8];
    char t28[8];
    char t32[8];
    char t41[8];
    char t45[8];
    char t54[8];
    char t58[8];
    char t67[8];
    char t71[8];
    char t80[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t29;
    char *t30;
    char *t31;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    char *t37;
    char *t38;
    char *t39;
    char *t40;
    char *t42;
    char *t43;
    char *t44;
    char *t46;
    char *t47;
    char *t48;
    char *t49;
    char *t50;
    char *t51;
    char *t52;
    char *t53;
    char *t55;
    char *t56;
    char *t57;
    char *t59;
    char *t60;
    char *t61;
    char *t62;
    char *t63;
    char *t64;
    char *t65;
    char *t66;
    char *t68;
    char *t69;
    char *t70;
    char *t72;
    char *t73;
    char *t74;
    char *t75;
    char *t76;
    char *t77;
    char *t78;
    char *t79;
    char *t81;

LAB0:    t1 = (t0 + 3736U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(24, ng0);
    t2 = (t0 + 4568);
    *((int *)t2) = 1;
    t3 = (t0 + 3768);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(25, ng0);

LAB5:    xsi_set_current_line(26, ng0);
    t4 = (t0 + 1368U);
    t5 = *((char **)t4);
    t4 = ((char*)((ng2)));
    memset(t6, 0, 8);
    t7 = (t5 + 4);
    t8 = (t4 + 4);
    t9 = *((unsigned int *)t5);
    t10 = *((unsigned int *)t4);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t7);
    t13 = *((unsigned int *)t8);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t7);
    t17 = *((unsigned int *)t8);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB9;

LAB6:    if (t18 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t6) = 1;

LAB9:    t22 = (t6 + 4);
    t23 = *((unsigned int *)t22);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB10;

LAB11:
LAB12:    goto LAB2;

LAB8:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(27, ng0);

LAB13:    xsi_set_current_line(28, ng0);
    t29 = (t0 + 2248);
    t30 = (t29 + 56U);
    t31 = *((char **)t30);
    t33 = (t0 + 2248);
    t34 = (t33 + 72U);
    t35 = *((char **)t34);
    t36 = (t0 + 2248);
    t37 = (t36 + 64U);
    t38 = *((char **)t37);
    t39 = (t0 + 1528U);
    t40 = *((char **)t39);
    t39 = ((char*)((ng7)));
    memset(t41, 0, 8);
    xsi_vlog_unsigned_add(t41, 32, t40, 8, t39, 32);
    xsi_vlog_generic_get_array_select_value(t32, 8, t31, t35, t38, 2, 1, t41, 32, 2);
    t42 = (t0 + 2248);
    t43 = (t42 + 56U);
    t44 = *((char **)t43);
    t46 = (t0 + 2248);
    t47 = (t46 + 72U);
    t48 = *((char **)t47);
    t49 = (t0 + 2248);
    t50 = (t49 + 64U);
    t51 = *((char **)t50);
    t52 = (t0 + 1528U);
    t53 = *((char **)t52);
    t52 = ((char*)((ng6)));
    memset(t54, 0, 8);
    xsi_vlog_unsigned_add(t54, 32, t53, 8, t52, 32);
    xsi_vlog_generic_get_array_select_value(t45, 8, t44, t48, t51, 2, 1, t54, 32, 2);
    t55 = (t0 + 2248);
    t56 = (t55 + 56U);
    t57 = *((char **)t56);
    t59 = (t0 + 2248);
    t60 = (t59 + 72U);
    t61 = *((char **)t60);
    t62 = (t0 + 2248);
    t63 = (t62 + 64U);
    t64 = *((char **)t63);
    t65 = (t0 + 1528U);
    t66 = *((char **)t65);
    t65 = ((char*)((ng2)));
    memset(t67, 0, 8);
    xsi_vlog_unsigned_add(t67, 32, t66, 8, t65, 32);
    xsi_vlog_generic_get_array_select_value(t58, 8, t57, t61, t64, 2, 1, t67, 32, 2);
    t68 = (t0 + 2248);
    t69 = (t68 + 56U);
    t70 = *((char **)t69);
    t72 = (t0 + 2248);
    t73 = (t72 + 72U);
    t74 = *((char **)t73);
    t75 = (t0 + 2248);
    t76 = (t75 + 64U);
    t77 = *((char **)t76);
    t78 = (t0 + 1528U);
    t79 = *((char **)t78);
    t78 = ((char*)((ng5)));
    memset(t80, 0, 8);
    xsi_vlog_unsigned_add(t80, 32, t79, 8, t78, 32);
    xsi_vlog_generic_get_array_select_value(t71, 8, t70, t74, t77, 2, 1, t80, 32, 2);
    xsi_vlogtype_concat(t28, 32, 32, 4U, t71, 8, t58, 8, t45, 8, t32, 8);
    t81 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t81, t28, 0, 0, 32, 0LL);
    goto LAB12;

}

static void impl1_execute(char *t0)
{
    char t7[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    t1 = (t0 + 3984U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    t2 = (t0 + 4584);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    t3 = (t0 + 2408);
    t4 = (t0 + 2248);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 2248);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 2248);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = (t0 + 1528U);
    t15 = *((char **)t14);
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t15, 8, 2);
    xsi_vlogimplicitvar_assign_value(t3, t7, 8);
    goto LAB2;

}

static void impl2_execute(char *t0)
{
    char t7[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    t1 = (t0 + 4232U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    t2 = (t0 + 4600);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    t3 = (t0 + 2568);
    t4 = (t0 + 2248);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 2248);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 2248);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = (t0 + 1528U);
    t15 = *((char **)t14);
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t15, 8, 2);
    xsi_vlogimplicitvar_assign_value(t3, t7, 8);
    goto LAB2;

}


extern void work_m_00000000003282298920_3854663782_init()
{
	static char *pe[] = {(void *)Always_11_0,(void *)Always_24_1,(void *)impl1_execute,(void *)impl2_execute};
	xsi_register_didat("work_m_00000000003282298920_3854663782", "isim/DMEM_256x8_isim_beh.exe.sim/work/m_00000000003282298920_3854663782.didat");
	xsi_register_executes(pe);
}
