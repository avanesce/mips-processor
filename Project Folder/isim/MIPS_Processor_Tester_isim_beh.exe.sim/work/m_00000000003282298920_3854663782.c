/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Andy/Documents/Xilinx Labs/Lab_7_MIPS_Processor/DMEM_256x8.v";
static int ng1[] = {1, 0};
static int ng2[] = {32, 0};
static int ng3[] = {24, 0};
static int ng4[] = {0, 0};
static int ng5[] = {2, 0};
static int ng6[] = {3, 0};



static void Always_11_0(char *t0)
{
    char t6[8];
    char t28[8];
    char t36[8];
    char t37[8];
    char t46[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    char *t38;
    char *t39;
    char *t40;
    char *t41;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    char *t47;
    unsigned int t48;
    int t49;
    char *t50;
    unsigned int t51;
    int t52;
    int t53;
    unsigned int t54;
    unsigned int t55;
    int t56;
    int t57;

LAB0:    t1 = (t0 + 3488U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(11, ng0);
    t2 = (t0 + 4552);
    *((int *)t2) = 1;
    t3 = (t0 + 3520);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(12, ng0);

LAB5:    xsi_set_current_line(13, ng0);
    t4 = (t0 + 1208U);
    t5 = *((char **)t4);
    t4 = ((char*)((ng1)));
    memset(t6, 0, 8);
    t7 = (t5 + 4);
    t8 = (t4 + 4);
    t9 = *((unsigned int *)t5);
    t10 = *((unsigned int *)t4);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t7);
    t13 = *((unsigned int *)t8);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t7);
    t17 = *((unsigned int *)t8);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB9;

LAB6:    if (t18 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t6) = 1;

LAB9:    t22 = (t6 + 4);
    t23 = *((unsigned int *)t22);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB10;

LAB11:
LAB12:    goto LAB2;

LAB8:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(14, ng0);

LAB13:    xsi_set_current_line(15, ng0);
    t29 = (t0 + 1688U);
    t30 = *((char **)t29);
    t29 = (t0 + 1648U);
    t31 = (t29 + 72U);
    t32 = *((char **)t31);
    t33 = ((char*)((ng2)));
    t34 = ((char*)((ng3)));
    xsi_vlog_generic_get_part_select_value(t28, 9, t30, t32, 2, t33, 32U, 1, t34, 32U, 1);
    t35 = (t0 + 2248);
    t38 = (t0 + 2248);
    t39 = (t38 + 72U);
    t40 = *((char **)t39);
    t41 = (t0 + 2248);
    t42 = (t41 + 64U);
    t43 = *((char **)t42);
    t44 = (t0 + 1528U);
    t45 = *((char **)t44);
    t44 = ((char*)((ng4)));
    memset(t46, 0, 8);
    xsi_vlog_unsigned_add(t46, 32, t45, 8, t44, 32);
    xsi_vlog_generic_convert_array_indices(t36, t37, t40, t43, 2, 1, t46, 32, 2);
    t47 = (t36 + 4);
    t48 = *((unsigned int *)t47);
    t49 = (!(t48));
    t50 = (t37 + 4);
    t51 = *((unsigned int *)t50);
    t52 = (!(t51));
    t53 = (t49 && t52);
    if (t53 == 1)
        goto LAB14;

LAB15:    xsi_set_current_line(16, ng0);
    t2 = (t0 + 1688U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t9 = *((unsigned int *)t3);
    t10 = (t9 >> 16);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 16);
    *((unsigned int *)t2) = t12;
    t13 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t13 & 255U);
    t14 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t14 & 255U);
    t5 = (t0 + 2248);
    t7 = (t0 + 2248);
    t8 = (t7 + 72U);
    t21 = *((char **)t8);
    t22 = (t0 + 2248);
    t29 = (t22 + 64U);
    t30 = *((char **)t29);
    t31 = (t0 + 1528U);
    t32 = *((char **)t31);
    t31 = ((char*)((ng1)));
    memset(t37, 0, 8);
    xsi_vlog_unsigned_add(t37, 32, t32, 8, t31, 32);
    xsi_vlog_generic_convert_array_indices(t28, t36, t21, t30, 2, 1, t37, 32, 2);
    t33 = (t28 + 4);
    t15 = *((unsigned int *)t33);
    t49 = (!(t15));
    t34 = (t36 + 4);
    t16 = *((unsigned int *)t34);
    t52 = (!(t16));
    t53 = (t49 && t52);
    if (t53 == 1)
        goto LAB16;

LAB17:    xsi_set_current_line(17, ng0);
    t2 = (t0 + 1688U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t9 = *((unsigned int *)t3);
    t10 = (t9 >> 8);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 8);
    *((unsigned int *)t2) = t12;
    t13 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t13 & 255U);
    t14 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t14 & 255U);
    t5 = (t0 + 2248);
    t7 = (t0 + 2248);
    t8 = (t7 + 72U);
    t21 = *((char **)t8);
    t22 = (t0 + 2248);
    t29 = (t22 + 64U);
    t30 = *((char **)t29);
    t31 = (t0 + 1528U);
    t32 = *((char **)t31);
    t31 = ((char*)((ng5)));
    memset(t37, 0, 8);
    xsi_vlog_unsigned_add(t37, 32, t32, 8, t31, 32);
    xsi_vlog_generic_convert_array_indices(t28, t36, t21, t30, 2, 1, t37, 32, 2);
    t33 = (t28 + 4);
    t15 = *((unsigned int *)t33);
    t49 = (!(t15));
    t34 = (t36 + 4);
    t16 = *((unsigned int *)t34);
    t52 = (!(t16));
    t53 = (t49 && t52);
    if (t53 == 1)
        goto LAB18;

LAB19:    xsi_set_current_line(18, ng0);
    t2 = (t0 + 1688U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t9 = *((unsigned int *)t3);
    t10 = (t9 >> 0);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 0);
    *((unsigned int *)t2) = t12;
    t13 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t13 & 255U);
    t14 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t14 & 255U);
    t5 = (t0 + 2248);
    t7 = (t0 + 2248);
    t8 = (t7 + 72U);
    t21 = *((char **)t8);
    t22 = (t0 + 2248);
    t29 = (t22 + 64U);
    t30 = *((char **)t29);
    t31 = (t0 + 1528U);
    t32 = *((char **)t31);
    t31 = ((char*)((ng6)));
    memset(t37, 0, 8);
    xsi_vlog_unsigned_add(t37, 32, t32, 8, t31, 32);
    xsi_vlog_generic_convert_array_indices(t28, t36, t21, t30, 2, 1, t37, 32, 2);
    t33 = (t28 + 4);
    t15 = *((unsigned int *)t33);
    t49 = (!(t15));
    t34 = (t36 + 4);
    t16 = *((unsigned int *)t34);
    t52 = (!(t16));
    t53 = (t49 && t52);
    if (t53 == 1)
        goto LAB20;

LAB21:    goto LAB12;

LAB14:    t54 = *((unsigned int *)t36);
    t55 = *((unsigned int *)t37);
    t56 = (t54 - t55);
    t57 = (t56 + 1);
    xsi_vlogvar_wait_assign_value(t35, t28, 0, *((unsigned int *)t37), t57, 0LL);
    goto LAB15;

LAB16:    t17 = *((unsigned int *)t28);
    t18 = *((unsigned int *)t36);
    t56 = (t17 - t18);
    t57 = (t56 + 1);
    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t36), t57, 0LL);
    goto LAB17;

LAB18:    t17 = *((unsigned int *)t28);
    t18 = *((unsigned int *)t36);
    t56 = (t17 - t18);
    t57 = (t56 + 1);
    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t36), t57, 0LL);
    goto LAB19;

LAB20:    t17 = *((unsigned int *)t28);
    t18 = *((unsigned int *)t36);
    t56 = (t17 - t18);
    t57 = (t56 + 1);
    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t36), t57, 0LL);
    goto LAB21;

}

static void Always_22_1(char *t0)
{
    char t6[8];
    char t28[8];
    char t32[8];
    char t41[8];
    char t45[8];
    char t54[8];
    char t58[8];
    char t67[8];
    char t71[8];
    char t80[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t29;
    char *t30;
    char *t31;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    char *t37;
    char *t38;
    char *t39;
    char *t40;
    char *t42;
    char *t43;
    char *t44;
    char *t46;
    char *t47;
    char *t48;
    char *t49;
    char *t50;
    char *t51;
    char *t52;
    char *t53;
    char *t55;
    char *t56;
    char *t57;
    char *t59;
    char *t60;
    char *t61;
    char *t62;
    char *t63;
    char *t64;
    char *t65;
    char *t66;
    char *t68;
    char *t69;
    char *t70;
    char *t72;
    char *t73;
    char *t74;
    char *t75;
    char *t76;
    char *t77;
    char *t78;
    char *t79;
    char *t81;

LAB0:    t1 = (t0 + 3736U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(22, ng0);
    t2 = (t0 + 4568);
    *((int *)t2) = 1;
    t3 = (t0 + 3768);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(23, ng0);

LAB5:    xsi_set_current_line(24, ng0);
    t4 = (t0 + 1368U);
    t5 = *((char **)t4);
    t4 = ((char*)((ng1)));
    memset(t6, 0, 8);
    t7 = (t5 + 4);
    t8 = (t4 + 4);
    t9 = *((unsigned int *)t5);
    t10 = *((unsigned int *)t4);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t7);
    t13 = *((unsigned int *)t8);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t7);
    t17 = *((unsigned int *)t8);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB9;

LAB6:    if (t18 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t6) = 1;

LAB9:    t22 = (t6 + 4);
    t23 = *((unsigned int *)t22);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB10;

LAB11:
LAB12:    goto LAB2;

LAB8:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(25, ng0);

LAB13:    xsi_set_current_line(26, ng0);
    t29 = (t0 + 2248);
    t30 = (t29 + 56U);
    t31 = *((char **)t30);
    t33 = (t0 + 2248);
    t34 = (t33 + 72U);
    t35 = *((char **)t34);
    t36 = (t0 + 2248);
    t37 = (t36 + 64U);
    t38 = *((char **)t37);
    t39 = (t0 + 1528U);
    t40 = *((char **)t39);
    t39 = ((char*)((ng6)));
    memset(t41, 0, 8);
    xsi_vlog_unsigned_add(t41, 32, t40, 8, t39, 32);
    xsi_vlog_generic_get_array_select_value(t32, 8, t31, t35, t38, 2, 1, t41, 32, 2);
    t42 = (t0 + 2248);
    t43 = (t42 + 56U);
    t44 = *((char **)t43);
    t46 = (t0 + 2248);
    t47 = (t46 + 72U);
    t48 = *((char **)t47);
    t49 = (t0 + 2248);
    t50 = (t49 + 64U);
    t51 = *((char **)t50);
    t52 = (t0 + 1528U);
    t53 = *((char **)t52);
    t52 = ((char*)((ng5)));
    memset(t54, 0, 8);
    xsi_vlog_unsigned_add(t54, 32, t53, 8, t52, 32);
    xsi_vlog_generic_get_array_select_value(t45, 8, t44, t48, t51, 2, 1, t54, 32, 2);
    t55 = (t0 + 2248);
    t56 = (t55 + 56U);
    t57 = *((char **)t56);
    t59 = (t0 + 2248);
    t60 = (t59 + 72U);
    t61 = *((char **)t60);
    t62 = (t0 + 2248);
    t63 = (t62 + 64U);
    t64 = *((char **)t63);
    t65 = (t0 + 1528U);
    t66 = *((char **)t65);
    t65 = ((char*)((ng1)));
    memset(t67, 0, 8);
    xsi_vlog_unsigned_add(t67, 32, t66, 8, t65, 32);
    xsi_vlog_generic_get_array_select_value(t58, 8, t57, t61, t64, 2, 1, t67, 32, 2);
    t68 = (t0 + 2248);
    t69 = (t68 + 56U);
    t70 = *((char **)t69);
    t72 = (t0 + 2248);
    t73 = (t72 + 72U);
    t74 = *((char **)t73);
    t75 = (t0 + 2248);
    t76 = (t75 + 64U);
    t77 = *((char **)t76);
    t78 = (t0 + 1528U);
    t79 = *((char **)t78);
    t78 = ((char*)((ng4)));
    memset(t80, 0, 8);
    xsi_vlog_unsigned_add(t80, 32, t79, 8, t78, 32);
    xsi_vlog_generic_get_array_select_value(t71, 8, t70, t74, t77, 2, 1, t80, 32, 2);
    xsi_vlogtype_concat(t28, 32, 32, 4U, t71, 8, t58, 8, t45, 8, t32, 8);
    t81 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t81, t28, 0, 0, 32, 0LL);
    goto LAB12;

}

static void impl1_execute(char *t0)
{
    char t7[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    t1 = (t0 + 3984U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    t2 = (t0 + 4584);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    t3 = (t0 + 2408);
    t4 = (t0 + 2248);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 2248);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 2248);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = (t0 + 1528U);
    t15 = *((char **)t14);
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t15, 8, 2);
    xsi_vlogimplicitvar_assign_value(t3, t7, 8);
    goto LAB2;

}

static void impl2_execute(char *t0)
{
    char t7[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    t1 = (t0 + 4232U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    t2 = (t0 + 4600);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    t3 = (t0 + 2568);
    t4 = (t0 + 2248);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 2248);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 2248);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = (t0 + 1528U);
    t15 = *((char **)t14);
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t15, 8, 2);
    xsi_vlogimplicitvar_assign_value(t3, t7, 8);
    goto LAB2;

}


extern void work_m_00000000003282298920_3854663782_init()
{
	static char *pe[] = {(void *)Always_11_0,(void *)Always_22_1,(void *)impl1_execute,(void *)impl2_execute};
	xsi_register_didat("work_m_00000000003282298920_3854663782", "isim/MIPS_Processor_Tester_isim_beh.exe.sim/work/m_00000000003282298920_3854663782.didat");
	xsi_register_executes(pe);
}
