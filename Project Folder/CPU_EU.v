`timescale 1ns / 1ps
module CPU_EU(	clk, RegDst, ALUSrc, ALUOp, RegWrite, MemtoReg,
						Instruction, SEImm, 
						RAM_Address, Data_to_RAM, Data_from_RAM,
						Zero);

	input				clk,
						RegDst,
						ALUSrc,
						RegWrite,
						MemtoReg;
	input	[1:0]		ALUOp;
	input	[25:0]	Instruction;
	input	[31:0]	Data_from_RAM;
	
	output[31:0]	SEImm,
						RAM_Address,
						Data_to_RAM;
	output			Zero;
	//Interconnecting Wires
	wire	[4:0]		WriteRegister;	//RegDstMux -> RF32
	wire	[3:0]		ALU_Ctrl;		//ALUCtrl -> ALU
	wire	[31:0]	ReadData1,		//RF32 -> ALU
						ReadData2,		//RF32 -> ALUSrcMux
						ALU_InputB, 	//ALUSrcMux -> ALU
						RegWriteData,	//MemToRegMux -> RF32
						ALU_Result;		//ALU -> MemToRegMux
	
	//Sub Module Instantiation
	//submodule for determining target register
	Mux2to1_5bit	RegDstMux	(
		.In0(Instruction[20:16]),
		.In1(Instruction[15:11]),
		.Sel(RegDst),
		.Out(WriteRegister)
	);
	//submodule for 32 bit register file
	RegFile32	RF32	(
		.Read1(Instruction[25:21]),
		.Read2(Instruction[20:16]),
		.WriteReg(WriteRegister),
		.WriteData(RegWriteData),
		.clock(clk),
		.RegWrite(RegWrite),
		.Data1(ReadData1),
		.Data2(ReadData2)
	);
	
	//submodule for determining data source
	Mux2to1_32bit	ALUSrcMux	(
		.In0(ReadData2),
		.In1(SEImm),
		.Sel(ALUSrc),
		.Out(ALU_InputB)
	);
	//submodule for loading data to write
	Mux2to1_32bit	MemToRegMux	(
		.In0(ALU_Result),
		.In1(Data_from_RAM),
		.Sel(MemtoReg),
		.Out(RegWriteData)
	);
	//submodule for signed extender
	SignExtend_16to32 SE32	(
		.SE_In(Instruction[15:0]),
		.SE_Out(SEImm)
	);
	//submodule for opcode procecssing
	ALU_Control	ALUCtrl	(
		.ALU_Op(ALUOp),
		.FuncCode(Instruction[5:0]),
		.ALU_Ctrl(ALU_Ctrl)
	);
	//submodule for ALU unit
	ALU	ALU32	(
		.A(ReadData1),
		.B(ALU_InputB),
		.ALU_Ctrl(ALU_Ctrl),
		.Zero_Flag(Zero),
		.Output(ALU_Result)
	);
	
	//Sets final ouputs
	assign RAM_Address = ALU_Result;	//RAM Address for sw, lw
	assign Data_to_RAM = ReadData2;	//value to be stored in RAM for sw
	
endmodule
