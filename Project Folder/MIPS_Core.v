`timescale 1ns / 1ps
module MIPS_Core(clk, ReadAddr, SEImm, JumpValue, Zero, Branch, Jump );
	input					clk;
	input 	[7:0]		ReadAddr;
	output	[31:0]	SEImm;
	output 	[25:0]	JumpValue;
	output				Zero,
							Branch,
							Jump;
	
	wire		[31:0]	I,
							RAM_Address,
							Data_to_RAM,
							Data_from_RAM;
							
	wire					RegDst, ALUSrc, MemtoReg, RegWrite,
							MemWrite, Branch, Jump, MemRead;
	wire		[1:0]		ALUOp;
	
	assign	JumpValue		= 	I[25:0];
	//Instruction Memory
	IMEM_128x8 IMEM(	.ReadAddress(		ReadAddr[7:0]),//input addr
							.Instruction(		I[31:0])			//output instr
							);
							
	Control		CU(	.Op( 					I[31:26]),		//input I
							.RegDst(				RegDst),			//to eu1
							.ALUSrc(				ALUSrc),
							.ALUOp(				ALUOp),
							.RegWrite(			RegWrite),
							.MemtoReg(			MemtoReg),
							.MemRead(			MemRead),		//to DMEM
							.MemWrite(			MemWrite),
							.Branch(				Branch),			//to pc counter
							.Jump(				Jump)
							);
							
	CPU_EU		eu1(	.clk(					clk),				//input clk
							.RegDst(				RegDst),			//from CU
							.ALUSrc(				ALUSrc),
							.ALUOp(				ALUOp),
							.RegWrite(			RegWrite),
							.MemtoReg(			MemtoReg),
							.Instruction(		I[25:0]),		//input I
							.SEImm(				SEImm[31:0]),	//output
							.RAM_Address(		RAM_Address),	//to DMEM
							.Data_to_RAM(		Data_to_RAM),	
							.Data_from_RAM(	Data_from_RAM),//from DMEM
							.Zero(				Zero)				//output zero
							);
	
	//Data Memory
	DMEM_256x8	DMEM(	.clock(				clk),
							.MemWrite(			MemWrite),
							.Address(			RAM_Address[7:0]),
							.WriteData(			Data_to_RAM),
							.MemRead(			MemRead),
							.ReadData(			Data_from_RAM)
							);

endmodule
