`timescale 1ns / 1ps

module Control( Op, RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp, Jump);
    input		[5:0]		Op;
	 
	 output reg				RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, Jump;
	 output reg	[1:0]		ALUOp;
	 
	 always @(Op) 	begin
		case(Op)
		6'h00:	{RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp, Jump}	=	10'b1001000100;
		6'h23:	{RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp, Jump}	=	10'b0111100000;
		6'h2b:	{RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp, Jump}	=	10'b0100010000;
		6'h04:	{RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp, Jump}	=	10'b0000001010;
		6'h02:	{RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp, Jump}	=	10'b0000000001;
		default: 	{RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp, Jump}	=	10'bX;
		endcase
		//$display(MemWrite);
	end

endmodule
