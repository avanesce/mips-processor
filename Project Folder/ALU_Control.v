`timescale 1ns / 1ps

module ALU_Control(ALU_Ctrl, ALU_Op, FuncCode);
   input		[5:0]		FuncCode;
	input		[1:0]		ALU_Op;
	 
	output reg	[3:0]		ALU_Ctrl;
	
	always @(ALU_Op, FuncCode) 	begin
		//$display("FuncCode: %b	ALUOp: %b", FuncCode, ALU_Op);
		casex({FuncCode,ALU_Op}) 
			8'b100000_10: ALU_Ctrl <= 4'b0010; //add
			8'b100010_10: ALU_Ctrl <= 4'b0110; //sub
			8'b100100_10: ALU_Ctrl <= 4'b0000; //and
			8'b100101_10: ALU_Ctrl <= 4'b0001; //or
			8'b101010_10: ALU_Ctrl <= 4'b0111; //slt
			8'b100111_10: ALU_Ctrl <= 4'b1100; //nor
			8'bXXXXXX_00: ALU_Ctrl <= 4'b0010; //load/store
			8'bXXXXXX_X1: ALU_Ctrl <= 4'b0110; //beq
			default:		  ALU_Ctrl <= 4'bxxxx;
		endcase
	end

endmodule